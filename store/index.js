import _ from 'lodash';
import vue from 'vue';

export const state = () => ({
  todos:[
    {id: 1, label: 'první', done: false},
    {id: 2, label: 'druhý', done: true},
    {id: 3, label: 'třetí', done: false}
  ]
});

export const mutations = {
  setTodos(state, todos){
    state.todos = todos
  },
  addTodo(state, todo){
    state.todos.push(todo)
  },
  editTodo(state, todo){
    const index = _.findKey(state.todos, {'id': todo.id})
    if(index === undefined){
      return
    }
    vue.set(state.todos, index, {'id': todo.id, 'label':todo.label, done:todo.done})
  },
  removeTodo(state, todoId) {
    const index = _.findKey(state.todos,{'id': todoId})
    if(index === undefined){
      return
    }
    vue.delete(state.todos, index)
  },
  changeStatus(state, todo){
    const index = _.findKey(state.todos, {'id': todo.id})
    if(index === undefined){
      return
    }
    vue.set(state.todos, index, {'id': todo.id, 'label':todo.label, 'done':todo.done})
  },

}
export const getters = {
  getTodos: state => done => {
    return _.filter(state.todos, {'done': done})
  }
}
